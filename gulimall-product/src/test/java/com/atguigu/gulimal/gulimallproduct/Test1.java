package com.atguigu.gulimal.gulimallproduct;

import com.atguigu.gulimall.product.GulimallProductApplication;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @description：
 * @author： chenjianlong
 * @date： 2021/5/20 14:54
 */
@SpringBootTest(classes = GulimallProductApplication.class)
public class Test1 {
    @Autowired
    public BrandService brandService;
    //测试数据库连接
    @Test
    void testAddBrand(){
        BrandEntity brand=new BrandEntity();
        brand.setName("雷神笔记本");
        brand.setDescript("雷神笔记本");
        brandService.save(brand);
    }
}
