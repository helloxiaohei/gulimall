package com.atguigu.gulimal.gulimallproduct;


import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimallProductApplicationTests {
    @Autowired
public BrandService brandService;
    @Test
    void contextLoads() {
    }
    @Test
    void testAddBrand(){
        BrandEntity brand=new BrandEntity();
        brand.setName("雷神笔记本");
        brand.setDescript("雷神笔记本");
        brandService.save(brand);
    }

}
