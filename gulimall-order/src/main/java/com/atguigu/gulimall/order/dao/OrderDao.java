package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author chenjianlong
 * @email 1158477486@qq.com
 * @date 2021-05-20 17:32:53
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
