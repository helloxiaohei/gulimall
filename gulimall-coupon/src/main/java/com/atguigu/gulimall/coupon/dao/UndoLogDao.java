package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenjianlong
 * @email 1158477486@qq.com
 * @date 2021-05-20 15:30:35
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
