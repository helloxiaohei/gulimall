package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author chenjianlong
 * @email 1158477486@qq.com
 * @date 2021-05-20 16:53:40
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
